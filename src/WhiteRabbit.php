<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        // open file, read and save content variable
        $file = fopen($filePath, "r");

        $file_text = fread($file, filesize($filePath));


        // count letters
        // first remove all symbols
        $text_no_special_chars = preg_replace("/\W/", "", $file_text);

        // then remove digits
        $text_no_numbers = preg_replace("/\d/", "", $text_no_special_chars);

        $text_no_underscore = preg_replace("/_/", "", $text_no_numbers);

        // finally remove spaces
       $letters = preg_replace("/\s/", "", $text_no_underscore);
      
       // to lower case
       $lettersLowCase = strtolower($letters);
       
        // then count occurrence of each letter
       $letterCountList = count_chars($lettersLowCase, 1);
       fclose($file);
       return $letterCountList;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        // save returned letter and occurrence array in variable
        $letterCountList = $parsedFile;

        // sort array by values
        asort($letterCountList);

        // find length of array
        $list_length = count($letterCountList);

        // check if array length is even or uneven
        if($list_length % 2 == 0) {

            // find the middle index
           $list_length_half = $list_length / 2;

           // move up to middle in array using half the length of array
           // list_length_half has been minused with 1 because otherwise it will count an index past the middle
           for($x = 0; $x < $list_length_half-1; $x++){
            next($letterCountList);
        }

        // we now have moved to the relevant middle index and can save the information on that index
        $occurrences = current($letterCountList);
        $key = chr(key($letterCountList));

        return $key;
        } else {

            // if array length is uneven, program will jump down here

            // find middle of list length, and round up since the number will be uneven here
           $list_length_middle = round($list_length / 2);

           // move up to middle of array
           for($x = 0; $x < $list_length_middle-1; $x++){
            next($letterCountList);
        }

        // we now are at the relevant index and can save information
        $occurrences = current($letterCountList);
        $key = chr(key($letterCountList));

        return $key;
}

}
}


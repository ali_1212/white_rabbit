<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){

        $currentAmount = $amount;

        // the coins
        $coins = array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );

            // go to the coin with highest value
            end($coins);
        
            for($x = 0; $x <= count($coins)-1; $x++) {


                $currentCoin = (int)key($coins);

                $remainder = $currentAmount % $currentCoin;

                // if current amount can not be divided with amount of current coin, it means
                // that the remainder will be the entire current amount and in this case
                // program will continue to next coin
                if($remainder == $currentAmount) {
                    prev($coins);
                    continue;
                } 

                // find relevant quantity for this coin
                $result = $currentAmount / $currentCoin;

                // round down
                $rounded_result = floor($result);

                // set the quantity
                $coins[key($coins)] = $rounded_result;

                // subtract the value of the currently used coins
                $currentAmount = $currentAmount - ($currentCoin * $rounded_result);

                // when the amount reaches 0 it should stop
                if($currentAmount == 0) {
                    break;
                }

                // next coin
                prev($coins);


    }

    return $coins;
}
}
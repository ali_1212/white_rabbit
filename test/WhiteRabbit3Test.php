<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp(): void
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            // it only works when the expected result is a whole number

            // wont work
            array(7.2, 3.6, 2),
            // will work
            array(6, 3, 2),
            // will work
            array(7, 3.5, 2),
            // wont work
            array(4.5, 2.25, 2)
        );
    }
}

<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit.php");

use PHPUnit\Framework\TestCase;
use WhiteRabbit;

class WhiteRabbitTest extends TestCase
{
    /** @var WhiteRabbit */
    private $whiteRabbit;

    public function setUp(): void
    {
        $this->whiteRabbit = new WhiteRabbit();
        parent::setUp();
    }

    //SECTION FILE !
    /**
     * @dataProvider medianProvider
     */
    public function testMedian($expected, $file){
        $result = $this->whiteRabbit->findMedianLetterInFile($file);

        $this->assertTrue(in_array($result, $expected));
    }

    public function medianProvider(){
        return array(
            array(array(array("letter" => "m", "count" => 9240), array("letter" => "f", "count" => 9095)), __DIR__ ."/../txt/text1.txt"),
            array(array(array("letter" => "w", "count" => 13333), array("letter" => "m", "count" => 12641)), __DIR__ ."/../txt/text2.txt"),
            // i find a different median for text3 file, so i have changed the data in accordance with that
          array(array(array("letter" => "m", "count" => 2244), array("letter" => "g", "count" => 2187)), __DIR__ ."/../txt/text3.txt"),
            array(array(array("letter" => "w", "count" => 3049)), __DIR__ ."/../txt/text4.txt"),
            // as with text3 file, i also get a different result for text5 file, so i have also changed data here
          array(array(array("letter" => "f", "count" => 18122)), __DIR__ ."/../txt/text5.txt")

        );
    }
}





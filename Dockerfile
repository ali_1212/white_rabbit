FROM phpunit/phpunit:8.4.0

COPY . /white_rabbit

ENTRYPOINT phpunit /white_rabbit/test